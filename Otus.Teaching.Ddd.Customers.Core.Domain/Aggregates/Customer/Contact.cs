﻿﻿using System;

 namespace Otus.Teaching.Ddd.Customers.Core.Domain.Aggregates.Customer
{
    /// <summary>
    /// Контакт клиента
    /// </summary>
    public class Contact
    {
        /// <summary>
        ///Id, Уникальный идентификатор
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Адрес электронной почты
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; private set; }
        
        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public Guid CustomerId { get; private set; }

        public Contact(Customer customer, string email, string phone)
        {
            Email = email;
            Phone = phone;
            Id = customer.Id;
        }
    }
}