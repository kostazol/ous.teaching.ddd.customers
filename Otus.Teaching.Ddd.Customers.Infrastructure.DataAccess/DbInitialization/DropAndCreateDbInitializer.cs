﻿﻿﻿using System;
using System.Collections.Generic;
  using Otus.Teaching.Ddd.Customers.Core.Domain.Aggregates.Customer;
  using Otus.Teaching.Ddd.Customers.Core.Infrastructure.DataAccess;

  namespace Otus.Teaching.Ddd.Customers.Core.Infrastructure.DataAccess.DbInitialization
{
    public class DropAndCreateDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dbContext;

        public DropAndCreateDbInitializer(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Initialize()
        {
            _dbContext.Database.EnsureDeleted();
            _dbContext.Database.EnsureCreated();

            var customers = new List<Customer>()
            {

            };

            _dbContext.Customers.AddRange(customers);

            _dbContext.SaveChanges();
        }
    }
}
